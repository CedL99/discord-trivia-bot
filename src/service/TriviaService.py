import requests
import html
from models.Question import Question
from models.Choice import Choice
class TriviaException(Exception): pass


class TriviaService():
        
    _instance = None

    @staticmethod
    def get_instance():
        if TriviaService._instance == None:
            TriviaService._instance = TriviaService()
        return TriviaService._instance


        
    def getTrivias(self, category = None, difficulty = None, amount = 1):



        if(category != None and int(category) not in Question.categories.values() ):
            raise ValueError("Invalid category")
        
        if(difficulty !=None and difficulty not in Question.difficultyList):
            raise ValueError("Invalid difficulty")
        
        if(amount <=0):
            raise ValueError("Amount must be greater than 0")


        query ="?amount=" + str(amount)        
        if(category !=None):
            query += "&category=" + str(category)

        if(difficulty != None):
            query += "&difficulty=" + str(difficulty)
        
        response = requests.get("https://opentdb.com/api.php" + query)
        if(response.status_code !=200):
            raise TriviaException("Request error")
        responseToJson = response.json()
        questionList=[]             
        for result in responseToJson['results']:
            incorrect_answers=[]
            for x in result['incorrect_answers']:
                incorrect_answers.append(html.unescape(x))

            question = Question(
                html.unescape(result['question']),
                result['difficulty'],
                result['category'],
                html.unescape(result['correct_answer']),
                incorrect_answers, 
                result['type'])
            questionList.append(question)

        return questionList
    
    def getTrivia(self, category = None, difficulty=None):
        return self.getTrivias(category, difficulty, 1)[0]
                        

    