import os
import psycopg2
from models.Question import Question
from models.Statistics import Statistics

class StatisticService():
    _instance = None
    
    @staticmethod
    def get_instance():
        if StatisticService._instance == None:
            StatisticService._instance = StatisticService()
        return StatisticService._instance
    
    def __init__(self):
        self.conn=psycopg2.connect(os.environ["DATABASE_URL"], sslmode='require')
        

    def test_connection(self):
        cur=self.conn.cursor()
        cur.execute("""SELECT version()""")
        x = cur.fetchone()[0]
        cur.close()
        return x

    def update_user_stats(self, guild_id,  user_id, difficulty, category, correct):
        
        if(self._does_stats_exists(guild_id,  user_id, difficulty, category)==False):
            self._init_user_stats(guild_id,  user_id, difficulty, category)
            

        if(correct):
            self._update_user_stats_correct(guild_id,  user_id, difficulty, category)
            
        else:
            self._update_user_stats_incorrect(guild_id,  user_id, difficulty, category)
            

    def _update_user_stats_correct(self, guild_id, user_id, difficulty, category):

        valueDictionnary = {"guild_id":guild_id, "user_id":user_id, "difficulty":difficulty, "category":category}
        query="UPDATE Statistics SET correct_answers = correct_answers + 1 WHERE user_id =%(user_id)s AND guild_id=%(guild_id)s AND category=%(category)s AND difficulty=%(difficulty)s;"
        cur = self.conn.cursor()
        cur.execute(query, valueDictionnary)
        self.conn.commit()
        cur.close()

    def _update_user_stats_incorrect(self, guild_id, user_id, difficulty, category):
        valueDictionnary = {"guild_id":guild_id, "user_id":user_id, "difficulty":difficulty, "category":category}
        query="UPDATE Statistics SET incorrect_answers = incorrect_answers + 1 WHERE user_id =%(user_id)s AND guild_id=%(guild_id)s AND category=%(category)s AND difficulty=%(difficulty)s;"
        cur = self.conn.cursor()
        cur.execute(query, valueDictionnary)
        self.conn.commit()
        cur.close()

    def _init_user_stats(self, guild_id, user_id, difficulty, category):
        valueDictionnary = {"guild_id":guild_id, "user_id":user_id, "difficulty":difficulty, "category":category}
        query = "INSERT INTO Statistics(user_id,guild_id,category,difficulty,correct_answers,incorrect_answers) Values(%(user_id)s,  %(guild_id)s, %(category)s, %(difficulty)s, 0, 0)"
        cur = self.conn.cursor()
        cur.execute(query, valueDictionnary)
        self.conn.commit()
        cur.close()

    def _does_stats_exists(self, guild_id, user_id, difficulty, category):
        cur=self.conn.cursor()
        valueDictionnary = {"guild_id":guild_id, "user_id":user_id, "difficulty":difficulty, "category":category}
        cur.execute("SELECT * FROM Statistics WHERE guild_id=%(guild_id)s AND user_id = %(user_id)s AND difficulty = %(difficulty)s AND category = %(category)s", 
        valueDictionnary)
        
        x=cur.fetchone()
        
        cur.close()
        
        if x==None:
            return False
        else:
            return True
        
    def getUserStats(self, user_id, category=None, difficulty=None):

        if(category != None and int(category) not in Question.categories.values()):
            raise ValueError("Invalid category")
        
        if(difficulty !=None and difficulty not in Question.difficultyList):
            raise ValueError("Invalid difficulty")


        cur=self.conn.cursor()
        valueDictionnary = {"user_id":user_id, "difficulty":difficulty, "category":category}
        query = "select SUM(correct_answers), SUM(incorrect_answers) from Statistics WHERE user_id =%(user_id)s"        
              
        if(category !=None):
            query += " AND category = %(category)s" 

        if(difficulty != None):
            query += " AND difficulty= %(difficulty)s " 

        cur.execute(query, valueDictionnary)
        row=cur.fetchone()
        if(row[0] is None and row[1] is None):
            return(Statistics(0,0))
        else:
            print(query)
            return(Statistics(row[0], row[1]))
                           
    




