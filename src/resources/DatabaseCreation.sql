--heroku pg:psql -f ./src/resources/DatabaseCreation.sql postgresql-objective-79503 --app cedrick-discord-bot



DROP TABLE IF EXISTS 
Statistics CASCADE;
CREATE TABLE Statistics (
	user_id NUMERIC(18),
	guild_id NUMERIC(18),
	category INT,
	difficulty varchar(50),
	correct_answers INT NOT NULL,
  	incorrect_answers INT  NOT NULL,
    PRIMARY KEY (user_id, guild_id, category, difficulty)
	);

INSERT INTO Statistics(user_id,guild_id,category,difficulty,correct_answers,incorrect_answers)
    Values(111111111111111111,111111111111111111, 2, 'medium', 2, 3);

INSERT INTO Statistics(user_id,guild_id,category,difficulty,correct_answers,incorrect_answers)
    Values(111111111111111111,111111111111111111, 2, 'hard', 1, 4);
INSERT INTO Statistics(user_id,guild_id,category,difficulty,correct_answers,incorrect_answers)
    Values(111111111111111111,111111111111111111, 2, 'eawdwsy', 1, 4);
