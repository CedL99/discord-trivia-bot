class Statistics():
    def __init__(self, correct_answers, incorrect_answers):
        self.correct_answers = correct_answers 
        self.incorrect_answers= incorrect_answers

    @property
    def average(self):

        if(self.total==0):
            return(0)
        else:
            return(self.correct_answers/self.total*100)
            

    @property
    def total(self):
        return(self.correct_answers + self.incorrect_answers)

    def __str__(self) -> str:
        return("Correct Answers : "+ str(self.correct_answers) + " Incorrect Answer : " + str(self.incorrect_answers) + " Total answers : " + str(self.total) + " Average : " + str(self.average))

