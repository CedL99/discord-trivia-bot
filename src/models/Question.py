import random

class Question():

    difficultyList=['easy', 'hard', 'medium']

    categories= {
        "General Knowledge" : 9,
        "Entertainment: Books" : 10,
        "Entertainment: Film" : 11,
        "Entertainment: Music" : 12,
        "Entertainment: Musicals & Theatres" : 13,
        "Entertainment: Television" : 14,
        "Entertainment: Video Games" : 15,
        "Entertainment: Board Games" : 16,
        "Science & Nature" : 17,
        "Science: Computers" : 18,
        "Science: Mathematics" : 19,
        "Mythology" : 20,
        "Sports" : 21,
        "Geography" : 22,
        "History" : 23,
        "Politics" : 24,
        "Art" : 25,
        "Celebrities" : 26,
        "Animals" : 27,
        "Vehicles" : 28,
        "Entertainment: Comics" : 29,
        "Science: Gadgets" : 30,
        "Entertainment: Japanese Anime & Manga" : 31,
        "Entertainment: Cartoon & Animations" : 32
    }

    def __init__(self, question, difficulty, category, correct_answer, incorrect_answers, type):
        self.question = question
        self.difficulty= difficulty
        self.category = category
        self.category_id = self.categories.get(category)
        self.correct_answer = correct_answer
        self.incorrect_answers= incorrect_answers
        self.type = type

    def __eq__(self, other):
        return \
            self.question == other.question and \
            self.difficulty == other.difficulty and \
            self.category == other.category and \
            self.correct_answer == other.correct_answer and \
            self.incorrect_answers == other.incorrect_answers
        

    def getAllAnswers(self):
        if(self.type == "boolean"):
            return ["True", "False"]

        all_answers = self.incorrect_answers + [self.correct_answer]

        random.shuffle(all_answers)
        return all_answers
    


    
        
        


