class ChannelVariables:

    _instance = None    

    @staticmethod    
    def get_instance():
        if ChannelVariables._instance == None:
            ChannelVariables._instance = ChannelVariables()
        return ChannelVariables._instance   
        
    def __init__(self):
        self._channelVariables = {}

    def get(self, channelId, var):
        return self._channelVariables[channelId][var]

    def set(self, channelId, var, val):
        if(channelId not in self._channelVariables):
            self._channelVariables[channelId] = {}
        self._channelVariables[channelId][var] = val   

    def has(self, channelId, var):
        if(channelId not in self._channelVariables):
            return False

        return var in self._channelVariables[channelId]
        
    def remove(self, channelId, var):
        del self._channelVariables[channelId][var]