import unittest
from models.Statistics import Statistics



class test_Statistics(unittest.TestCase):
    def test_average(self):
        parameters = (
            (1,3,25),
            (2,8,20),
            (0,10,0),
            (10,0,100),
            (0,0,0)
        )
        for correct, incorrect, average in parameters:
            stats = Statistics(correct, incorrect)
            self.assertEqual(stats.average, average)

    def test_total(self):
        stats = Statistics(1,3)
        self.assertEqual(stats.total, 4)