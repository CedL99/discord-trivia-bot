import unittest
from unittest import mock

from service.TriviaService import TriviaService
from service.TriviaService import TriviaException
from models.Question import Question

class ValidResponse:
    status_code=200
    def json(self):
        return {
    "response_code": 0,
    "results": [
        {
            "category": "Sports",
            "type": "multiple",
            "difficulty": "hard",
            "question": "Which male player won the gold medal of table tennis singles in 2016 Olympics Games?",
            "correct_answer": "Ma Long (China)",
            "incorrect_answers": [
                "Zhang Jike (China)",
                "Jun Mizutani (Japan)",
                "Vladimir Samsonov (Belarus)"
            ]
        }
    ]
}
class InvalidResponse:
    status_code=500
    

class test_TriviaService(unittest.TestCase):
    
    invalid_categories = [1,2,3,4,5,6,7,122,55,56,76, -3, -4,-54444,-4, 'category', 'blabla', "multiple"]
    validQuestion = Question("Which male player won the gold medal of table tennis singles in 2016 Olympics Games?",
                            "hard",
                            "Sports",
                           "Ma Long (China)",
                            [
                                "Zhang Jike (China)",
                                "Jun Mizutani (Japan)",
                                "Vladimir Samsonov (Belarus)"
                            ],
                            "multiple")

    def setUp(self):
        self.triviaService = TriviaService.get_instance()


    def test_invalid_category(self):
        for value in self.invalid_categories:
            with self.assertRaises(ValueError):
                self.triviaService.getTrivia(category=value)

    def test_invalid_diffculty(self):
        self.assertRaises(ValueError)
        with self.assertRaises(ValueError):
            self.triviaService.getTrivia(difficulty="bad")

    def test_invalid_amount(self):
        self.assertRaises(ValueError)
        with self.assertRaises(ValueError):
            self.triviaService.getTrivias(amount = -2)

    @mock.patch("requests.get")
    def test_return_valid_object(self, mock_request_get):
        mock_request_get.return_value = ValidResponse()
        test_question = self.triviaService.getTrivia()
        self.assertEqual(test_question, self.validQuestion)

    @mock.patch("requests.get")
    def test_API_error(self, mock_request_get):
        mock_request_get.return_value = InvalidResponse()
        with self.assertRaises(TriviaException):
            self.triviaService.getTrivia()
            
        