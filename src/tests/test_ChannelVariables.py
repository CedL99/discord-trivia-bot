import unittest
from core.ChannelVariables import ChannelVariables

class test_ChannelVariables(unittest.TestCase):

    def setUp(self):
        self.channelVariables = ChannelVariables()


    def test_get_and_set(self):
        
        self.channelVariables.set(23322, "question", "this is a question")
        self.assertEqual("this is a question", self.channelVariables.get(23322, "question") )

    def test_has_true(self):

        self.channelVariables.set(23322, "question", "this is a question")
        self.assertTrue(self.channelVariables.has(23322, "question"))

    def test_has_false(self):

        self.assertFalse(self.channelVariables.has(23322, "question"))

    def test_remove(self):

        self.channelVariables.set(23322, "question", "this is a question")
        self.channelVariables.remove(23322, "question")

        self.assertFalse(self.channelVariables.has(23322, "question"))
