from discord.ext import commands
from discord.ext.commands.core import command
from core.ChannelVariables import ChannelVariables
from models.Question import Question
from models.Statistics import Statistics
from service.TriviaService import TriviaService
from service.TriviaService import TriviaException
from service.StatisticService import StatisticService

class TriviaCog(commands.Cog):

    

    def __init__(self, bot):
        self.bot = bot
        self.triviaService = TriviaService.get_instance()
        self.channelVariables = ChannelVariables.get_instance()
        self.statisticService = StatisticService.get_instance()
    

    @commands.command()
    async def ping(self, ctx):
        await ctx.send(f'Pong')
            
    
    
    
    @commands.command(
        name="Trivia",
        aliases=["t"],
        usage="[Category | Difficulty]"
    )
    async def triviaCommand(self, ctx, *args):

        category=None
        difficulty=None
        for arg in args:
            if(self.is_difficulty(arg)):
                difficulty= arg
            elif(self.is_category(arg)):
                category = arg
            else:
                await ctx.send("Invalid arguments")
                return

        channel_id = ctx.channel.id
        if self.channelVariables.has(channel_id, "question"):
            return
        
        try:
            question = self.triviaService.getTrivia(category=category, difficulty=difficulty)  

            self.setQuestion(channel_id, question)

            await self.sendQuestion(ctx)

            self.bot.add_listener(self.answerListener, 'on_message')
                    
        except TriviaException as e:
            await ctx.send("Problem connecting to services")
        except ValueError as e:
            await self.sendError(ctx, e)
        except Exception as e:
            await self.sendError(ctx, e)
        
        
        

        
    async def sendQuestion(self, ctx):
        msg = self.channelVariables.get(ctx.channel.id, "question").question + "\n"
        i = 1
        for choice in self.channelVariables.get(ctx.channel.id, "all_answers"):
            msg += f"{i}. {choice} \n"
            i += 1
        await ctx.send(msg)

    async def sendError(self, ctx, e):
        await ctx.send(str(e))
    
    async def answerListener(self, message):

        if(message.author.bot):
            return

        if(not self.channelVariables.has(message.channel.id, "question")):
            return

        all_answer_list = self.channelVariables.get(message.channel.id, "all_answers")

        question = correctAnswer = self.channelVariables.get(message.channel.id, "question")

        correctAnswer = question.correct_answer

        userMessage = message.content.lower()

        if(userMessage==correctAnswer.lower() or str(all_answer_list.index(correctAnswer)+ 1)  == userMessage):           
            await message.channel.send("Correct" )  
            self.statisticService.update_user_stats(
                message.guild.id, 
                message.author.id, question.difficulty, 
                question.category_id, 
                True
                 )            
        else:
            await message.channel.send("Incorrect. The answer was : " + correctAnswer)
            self.statisticService.update_user_stats(
                message.guild.id, 
                message.author.id, 
                question.difficulty, 
                question.category_id,
                False 
                ) 
            
        self.channelVariables.remove(message.channel.id, "question")
        self.bot.remove_listener(self.answerListener, 'on_message')
        
        
    @commands.command(
        name="Categories",
        aliases=["c"],
        
    )    
    async def display_categories(self, ctx):
            categories_string="__**Categories**__ \n"
            for key, value in Question.categories.items():
                categories_string+= str(value) +". " + key + "\n"
            await ctx.send(categories_string)    
    
    def setQuestion(self, channelId, question):
        self.channelVariables.set(channelId, "question", question)
        self.channelVariables.set(channelId, "all_answers", question.getAllAnswers())

    
    @commands.command(
        name="Stats",
        aliases=["ts"],
        usage="[User | Category | Difficulty]"
    )
    async def display_stats(self, ctx, *args):
        
        user_name = None
        user_id = None
        category = None
        difficulty= None


        for arg in args:
            if(self.is_difficulty(arg)):
                difficulty= arg
            elif(self.is_category(arg)):
                category = arg
            elif(self.is_user(ctx, arg)):
                
                member = ctx.guild.get_member_named(arg)
                user_id = member.id
                user_name = member.name
            else:
                await ctx.send("invalid arguments")
                return

        if(user_id is None):
            user_id = ctx.author.id

        if self.channelVariables.has(ctx.channel.id, "question"):
            return
        
        stringCategory=None
        for key, value in Question.categories.items(): 
            if category==str(value):
                stringCategory=key
                

        try:

            stats = self.statisticService.getUserStats(user_id, category, difficulty)
            msg = "**__Stats"
            if(category != None):
                msg+= " for the category " + stringCategory

            if(difficulty != None):
                msg+= " on the difficulty " + difficulty
            if(user_id != ctx.author.id):
                msg+= " for " + user_name
            msg+="__**"           
            
            msg+="\n Correct answers : " + str(stats.correct_answers) + "\n Incorrect answers : " + str(stats.incorrect_answers) + "\n Total Answers :" + str(stats.total) + "\n Average : {:.2f}".format(stats.average)
            await ctx.send(msg)                
            
        except ValueError as e:
            await self.sendError(ctx,e)
        
    def is_user(self, ctx, user):
        if (ctx.guild.get_member_named(user) != None):
            return True
        else:
            return False

    def is_category(self, category):
        if (category) in str(Question.categories.values()):
            return True
        else: 
            return False

    def is_difficulty(self, difficulty):
        if difficulty in Question.difficultyList:
            return True
        else:
            return False
            
    
   

    

        