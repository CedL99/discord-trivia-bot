import os
import discord
from discord.ext import commands
from dotenv import load_dotenv
from Controllers.TriviaCommands import TriviaCog

load_dotenv()

def main():

    intents = discord.Intents.default()
    intents.members = True
    bot = commands.Bot(intents=intents, command_prefix='!', case_insensitive=True)

    bot.add_cog(TriviaCog(bot))
    
    @bot.event
    async def on_ready():
        print('We have logged in as {0.user}'.format(bot))
    bot.run(os.environ["DISCORD_TOKEN"])

main()



