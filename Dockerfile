FROM python:3.9.6-slim

#Copy files from project in app
COPY . /app
#creating directory for the app
WORKDIR /app
#installing requirements
RUN pip install -r requirements.txt


CMD [ "python3", "./src/main.py"]